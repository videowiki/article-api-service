const Article = require('./models').Article;
const utils = require('./utils');

module.exports = router => {


    router.post('/:id/slides/:slidePosition/content/:subslidePosition', (req, res) => {
        const { id, slidePosition, subslidePosition } = req.params;
        const subslide = req.body;
        utils.addSubslide(id, slidePosition, subslidePosition, subslide)
            .then(r => res.json(r))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id/slides/:slidePosition/content/:subslidePosition', (req, res) => {
        const changes = req.body;
        const { id, slidePosition, subslidePosition } = req.params;
        utils.updateSubslideUsingPosition(id, slidePosition, subslidePosition, changes)
            .then((changes) => res.json(changes))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            });
    })

    router.delete('/:id/slides/:slidePosition/content/:subslidePosition', (req, res) => {
        const { id, slidePosition, subslidePosition } = req.params;
        utils.removeSubslide(id, slidePosition, subslidePosition)
            .then(r => res.json(r))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            });
    })

    router.post('/:id/slides/:slidePosition/content/:subslidePosition/split', (req, res) => {
        const { id, slidePosition, subslidePosition } = req.params;
        const { wordIndex, time } = req.body;

        utils.splitSubslide(id, slidePosition, subslidePosition, wordIndex, time)
            .then(r => res.json(r))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            });
    })

    router.post('/:id/find_and_replace', (req, res) => {
        const { id } = req.params;
        const { find, replace } = req.body;
        utils.replaceArticleSlidesText(id, { find, replace })
            .then((changedSlides) => {
                return res.json(changedSlides)
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            });
    })

    router.post('/:id/clone', (req, res) => {
        Article.findById(req.params.id)
            .then((article) => {
                article = article.toObject();
                article.originalArticle = article._id;
                delete article._id;
                return Article.create(article);
            })
            .then(article => {
                return res.json(article);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/count', (req, res) => {
        const query = req.query;
        Object.keys(query).forEach((key) => {
            if (key && key.indexOf && key.indexOf('$') === 0) {
                const val = query[key];
                query[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        query[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        Article.count(query)
            .then(count => res.json(count))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message)
            })

    })



    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest } = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }

        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        if (one) {
            q = Article.findOne(rest);
        } else {
            q = Article.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((articles) => {
            return res.json(articles);
        })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.post('/', (req, res) => {
        const data = req.body;
        Article.create(data)
            .then((article) => {
                return res.json(article);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        Article.update(conditions, { $set: values }, { ...options, multi: true })
            .then(() => Article.find(conditions))
            .then(articles => {
                return res.json(articles);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/', (req, res) => {
        let conditions = req.body;
        let articles;
        Article.find(conditions)
            .then((a) => {
                articles = a;
                return Article.remove(conditions)
            })
            .then(() => {
                return res.json(articles);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/:id', (req, res) => {
        Article.findById(req.params.id)
            .then((article) => {
                return res.json(article);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        Article.findByIdAndUpdate(id, { $set: changes })
            .then(() => Article.findById(id))
            .then(article => {
                return res.json(article);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/:id', (req, res) => {
        const { id } = req.params;
        let deletedArticle;
        Article.findById(id)
            .then(article => {
                deletedArticle = article;
                return Article.findByIdAndRemove(id)
            })
            .then(() => {
                return res.json(deletedArticle);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    return router;
}
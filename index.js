const mongoose = require('mongoose');
const videowikiGenerators = require('@videowiki/generators');
const { server, app, createRouter } = require('./generateServer')();
const controller = require('./controller');
const middlewares = require('./middlewares');

const DB_CONNECTION = process.env.ARTICLE_SERVICE_DATABASE_URL;
let mongoConnection;
mongoose.connect(DB_CONNECTION)
.then(con => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection })

    app.use('/db', require('./dbRoutes')(createRouter()))

    app.all('*', (req, res, next) => {
        if (req.headers['vw-user-data']) {
            try {
                const user = JSON.parse(req.headers['vw-user-data']);
                req.user = user;
            } catch (e) {
                console.log(e);
            }
        }
        next();
    })


    app.get('/whatsapp-bot', controller.getArticleForWhatsApp);
    app.get('/by_video_id', controller.getByVideoId);
    app.get('/translations', controller.getArticlesTranslations);

    app.get('/translations/count', controller.getTranslationsCount)
    // TODO: DOC THIS
    app.get('/translations/by_user', controller.getUserTranslations)
    app.get('/translations/single', controller.getSingleArticleTranslations);

    app.get('/transcriptionVersions', controller.getTranscriptionVersions)
    app.post('/:articleId/transcriptionVersions/setTranscriptionVersionForSubslide', controller.setTranscriptionVersionForSubslide)
    app.post('/:articleId/transcriptionVersions/setTranscriptionVersionForAllSubslides', controller.setTranscriptionVersionForAllSubslides)

    // proofreading stage update routes
    app.post('/:articleId/slides/:slidePosition/content/:subslidePosition/split', middlewares.authorizeArticleUpdate, controller.splitSubslide);
    app.patch('/:articleId/slides/:slidePosition/content/:subslidePosition', middlewares.authorizeArticleUpdate, controller.updateSubslide);
    app.post('/:articleId/slides/:slidePosition/content/:subslidePosition', middlewares.authorizeArticleUpdate, controller.addSubslide);
    app.delete('/:articleId/slides/:slidePosition/content/:subslidePosition', middlewares.authorizeArticleUpdate, controller.deleteSubslide);

    app.post('/:articleId/text/replace', middlewares.authorizeArticleUpdate, controller.replaceArticleText);
    app.post('/:articleId/automatedBreak', middlewares.authorizeArticleUpdate, controller.automaticBreakArticle);

    app.post('/:articleId/subscribeAITranscribeFinish', middlewares.authorizeArticleUpdate, controller.subscribeAITranscribeFinish);

    // app.post('/:articleId/slides/text/replace', )
    app.put('/:articleId/speakersProfile', middlewares.authorizeArticleUpdate, controller.updateSpeakersProfile);

    app.put('/:articleId/toEnglish', middlewares.authorizeArticleUpdate, controller.updateToEnglish);
    app.put('/:articleId/reviewCompleted', middlewares.authorizeArticleUpdate, controller.updateReviewCompleted);

    app.put('/:articleId/translators', middlewares.authorizeArticleAdmin, controller.updateTranslators)
    app.put('/:articleId/textTranslators', middlewares.authorizeArticleAdmin, controller.updateTextTranslators)
    app.put('/:articleId/projectLeaders', middlewares.authorizeArticleAdmin, controller.updateProjectLeaders)

    app.put('/:articleId/verifiers', middlewares.authorizeArticleAdmin, controller.updateVerifiers)
    app.post('/:articleId/verifiers/resendEmail', middlewares.authorizeArticleAdmin, controller.resendEmailToVerifier)

    app.patch('/:articleId/translators/finishDate', middlewares.authorizeFinishdateUpdate, controller.updateTranslatorFinishDate)

    // TODO: DOC THIS
    app.get('/:articleId/comments', controller.getArticleComments);
    app.get('/', controller.getArticles)
    app.get('/:articleId', controller.getById);
    app.delete('/:articleId', middlewares.authorizeArticleAdmin, controller.deleteArticle)

})
.catch(err => {
    console.log('error initiating mongodb connection', err)
    process.exit(1);
})

const PORT = process.env.PORT || 4000;

server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
